/**
 * Created by ESER.DANACIOGLU on 2.11.2016.
 */
var lcwNavigation = (function () {
    function lcwNavigation(position, data, isSearch, isActive) {
        this._position = position;
        this._data = data;
        this._body = document.body;
        this._isSearch = isSearch;
        this._isActive = isActive;
        this._item = document.createElement("DIV");
        this._cons = document.createElement("DIV");
        this._cons.className = "nav";
        this.lcwNavGenerateElements(this._data);
    }
    lcwNavigation.prototype.lcwNavGenerateElements = function (data) {
        this._data = data;
        this._cons.innerHTML = null;
        for (var item in this._data) {
            this._element = document.createElement("DIV");
            this._itemText = this._data[item]["name"];
            this._element.innerText = this._itemText;
            this._cons.appendChild(this._element);
        }
        this._body.appendChild(this._cons);
    };
    //
    lcwNavigation.prototype.lcwNavClose = function () {
    };
    lcwNavigation.prototype.lcwNavOpen = function () {
    };
    lcwNavigation.prototype.lcwNavDestroy = function () {
        this._cons.remove();
        delete this._data;
        delete this._cons;
        delete this._element;
        delete this._isActive;
        delete this._isSearch;
        delete this._item;
        delete this._itemText;
        delete this._position;
        delete this._body;
    };
    return lcwNavigation;
}());
var data = [
    {
        "id": 0,
        "name": "Eula Shepard",
        "link": "Ex ut exercitation voluptate fugiat ullamco."
    },
    {
        "id": 1,
        "name": "Hughes Velez",
        "link": "Ea ea cupidatat culpa sit dolor aliqua."
    },
    {
        "id": 2,
        "name": "Amy Serrano",
        "link": "Adipisicing consectetur dolor anim minim nisi mollit enim commodo duis Lorem."
    },
    {
        "id": 3,
        "name": "Penelope Macdonald",
        "link": "Proident duis laborum amet incididunt eiusmod velit duis id pariatur sint reprehenderit reprehenderit."
    },
    {
        "id": 4,
        "name": "Fischer Rosales",
        "link": "Deserunt nulla occaecat occaecat sint in duis officia cupidatat eu incididunt."
    },
    {
        "id": 5,
        "name": "Lana Rich",
        "link": "Esse irure velit anim exercitation nostrud irure irure veniam incididunt esse voluptate deserunt ad."
    },
    {
        "id": 6,
        "name": "Greer Snyder",
        "link": "Esse commodo ipsum commodo in id nostrud incididunt aliqua occaecat aliquip ad consequat id."
    },
    {
        "id": 7,
        "name": "Reyes York",
        "link": "Mollit minim officia amet sit occaecat dolor consectetur anim ad mollit esse aliquip."
    },
    {
        "id": 8,
        "name": "Thomas Mooney",
        "link": "Dolor nulla incididunt aute eiusmod sint ullamco qui minim incididunt tempor ex dolore fugiat."
    },
    {
        "id": 9,
        "name": "Estrada Henson",
        "link": "Culpa laboris excepteur sit non ea minim reprehenderit eiusmod dolor pariatur tempor eiusmod eu aute."
    },
    {
        "id": 10,
        "name": "Constance Fry",
        "link": "Minim culpa commodo id nisi."
    },
    {
        "id": 11,
        "name": "Shaw Cross",
        "link": "Nulla magna elit anim non officia laboris ex labore est nisi esse."
    },
    {
        "id": 12,
        "name": "Teresa Roberson",
        "link": "Ex est sunt nisi fugiat mollit."
    },
    {
        "id": 13,
        "name": "Shana Long",
        "link": "Ea veniam eiusmod mollit dolor officia do dolor esse incididunt."
    },
    {
        "id": 14,
        "name": "Moon Osborne",
        "link": "Lorem exercitation et ea aliquip pariatur."
    }
];
var data2 = [
    {
        "id": 0,
        "name": "Haböle",
        "link": "Ex ut exercitation voluptate fugiat ullamco."
    },
    {
        "id": 1,
        "name": "Hughes Velez",
        "link": "Ea ea cupidatat culpa sit dolor aliqua."
    },
    {
        "id": 2,
        "name": "Amy Serrano",
        "link": "Adipisicing consectetur dolor anim minim nisi mollit enim commodo duis Lorem."
    },
    {
        "id": 3,
        "name": "Penelope Macdonald",
        "link": "Proident duis laborum amet incididunt eiusmod velit duis id pariatur sint reprehenderit reprehenderit."
    },
    {
        "id": 4,
        "name": "Fischer Rosales",
        "link": "Deserunt nulla occaecat occaecat sint in duis officia cupidatat eu incididunt."
    },
    {
        "id": 5,
        "name": "Lana Rich",
        "link": "Esse irure velit anim exercitation nostrud irure irure veniam incididunt esse voluptate deserunt ad."
    },
    {
        "id": 6,
        "name": "Greer Snyder",
        "link": "Esse commodo ipsum commodo in id nostrud incididunt aliqua occaecat aliquip ad consequat id."
    },
    {
        "id": 7,
        "name": "Reyes York",
        "link": "Mollit minim officia amet sit occaecat dolor consectetur anim ad mollit esse aliquip."
    },
    {
        "id": 8,
        "name": "Thomas Mooney",
        "link": "Dolor nulla incididunt aute eiusmod sint ullamco qui minim incididunt tempor ex dolore fugiat."
    },
    {
        "id": 9,
        "name": "Estrada Henson",
        "link": "Culpa laboris excepteur sit non ea minim reprehenderit eiusmod dolor pariatur tempor eiusmod eu aute."
    },
    {
        "id": 10,
        "name": "Constance Fry",
        "link": "Minim culpa commodo id nisi."
    },
    {
        "id": 11,
        "name": "Shaw Cross",
        "link": "Nulla magna elit anim non officia laboris ex labore est nisi esse."
    },
    {
        "id": 12,
        "name": "Teresa Roberson",
        "link": "Ex est sunt nisi fugiat mollit."
    },
    {
        "id": 13,
        "name": "Shana Long",
        "link": "Ea veniam eiusmod mollit dolor officia do dolor esse incididunt."
    },
    {
        "id": 14,
        "name": "Moon Osborne",
        "link": "Lorem exercitation et ea aliquip pariatur."
    }
];
var nav = new lcwNavigation("left", data, true, true);
//# sourceMappingURL=sidNav.js.map